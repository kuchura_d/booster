module.exports = {
    stylesheets: true,

    javascripts: {
        entry: {
            app: ["./index.js"]
        }
    },

    browserSync: {
        proxy: {
            target: 'boosters.loc'
        }
    },

    production: {
        rev: false
    }
};