# Sample Laravel Application

#### Manual
- Clone repo
- Install composer dependencies: `composer install`
- After install automatically create `.env file` you must change `DB_HOST, DB_DATABASE, DB_USERNAME and DB_PASSWORD`
- And after configure Database run commands:
    - `php artisan migrate` (create tables)
    - `php artisan db:seed` (seeding this tables)
- Browse the work

### Used

 - PHP `7.1` version;
 - MySQL `5.7` version (5.7 because default set mode: `only_full_group_by`);
 - Faker (for random data generated);
 - Bootstrap 3 (for UI);
 - JQuery `3.2.1` version;
 - Blendid `4.3` for minify my .css and .js files;
 
### Directory Structure

```
_html                directory for Yarn
    config/               configurations files for Yarn and Blendid
    node_modules/         (ignored)
    public/               contains compiled .js and .css files 
    src/                  contains my original .js and .scss files
        javascript/       **in file index.js writen all javascript (define JQuery, Toastr noty etc.)**
        stylesheets/      includes scss files for Toastr, Bootsrap etc.
        
app                 application files Laravel (Controllers and Models)
    Http/                 this folder contains my controllers
         Controllers/       ** ApiController  - controller for update and create data in database
                            ** Controller - it's my Base Controller
                            ** IndexController - contains method Index page
         Widgets/         in this folder contains four widgets for view partials index page
                          ** Companies - model for table `companies`
                          ** Countries - model for table `countries`
                          ** Leaders - model for table `leaders`
                          
config              contains configurations files
database            contains migrations files and seeds
    migrations/           for migrate 3 tables companies, countries, leaders
    seeds/                seed's files for tables companies, countries
public              the entry script and Web resources
resourse            contains views files
    api/                  for ajax render data
    forms/                forms to create countries and companies
    widgets/              view files my widgets
routes              contains routes web app
vendor              vendor folder dependencies
```

### About work
Work contains Index page where you must see 3 tabs: Companies, Countries, Leaders.

Tabs Countries and Companies has differents buttons Add/Edit/Delete. Add/Edit call to form in modal window,
Delete drop current item from database and page

All function contains in `_html/src/javascript/index.js` and **compiled/minify** to `public/js/app.js`

Thank you!!!