<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use App\Leaders as Model;

class Leaders extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $result = Model::limit(20)->get();
        $month = Model::month();

        return view('widgets.leaders', [
            'result' => $result,
            'month' => $month,
            'config' => $this->config,
        ]);
    }
}
