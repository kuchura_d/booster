<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use App\Countries as Model;

class Countries extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $result = Model::all();

        return view('widgets.countries', [
            'result' => $result,
            'config' => $this->config,
        ]);
    }
}
