<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use App\Companies as Model;

class Companies extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $result = Model::all();

        return view('widgets.companies', [
            'result' => $result,
            'config' => $this->config,
        ]);
    }
}
