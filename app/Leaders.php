<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Leaders
 * @package App
 *
 * @property integer $id
 * @property integer $company_id
 * @property integer $date_time
 * @property integer $mined
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Companies $company
 */
class Leaders extends Model
{
    /**
     * @var string
     */
    protected $table = 'leaders';

    public function company()
    {
        return $this->hasOne(Companies::class, 'id', 'company_id');
    }

    public static function month()
    {
        return [
            1 => 'January',
            2 => 'February',
            3 => 'March',
            4 => 'April',
            5 => 'May',
            6 => 'June',
            7 => 'July',
            8 => 'August',
            9 => 'September',
            10 => 'October',
            11 => 'November',
            12 => 'December',
        ];
    }
}
