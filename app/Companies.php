<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Companies
 * @package App
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property integer $country_id
 *
 * @property Countries $country
 */
class Companies extends Model
{
    /**
     * @var string
     */
    protected $table = 'companies';

    public static function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required',
            'country_id' => 'required',
        ];
    }

    public function country()
    {
        return $this->hasOne(Countries::class, 'id', 'country_id');
    }
}
