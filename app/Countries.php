<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Countries
 * @package App
 *
 * @property integer $id
 * @property string $name
 * @property integer $quota
 * @property integer $quota_type
 * @property string $created_at
 * @property string $updated_at
 */
class Countries extends Model
{
    /**
     * @var string
     */
    protected $table = 'countries';

    public static function rules()
    {
        return [
            'name' => 'required',
            'quota' => 'required',
            'quota_type' => 'required',
        ];
    }

    /**
     * Types of weight
     *
     * @return array
     */
    public static function types()
    {
        return [
            'Kg' => 'Kilogram',
            'T' => 'Tonnes'
        ];
    }

    /**
     * In this method we get countries list as array
     *
     * @return array
     */
    public static function getCountriesAsArray()
    {
        $result = Countries::all();

        $countries = [];

        foreach ($result as $obj) {
            $countries[$obj->id] = $obj->name;
        }

        return $countries;
    }

    /**
     * Converts human readable quota size (e.g. 10 gram, 200.20 kilograms) into Kilograms, Tonnes
     *
     * @param int $grams
     * @return int|string
     */
    public static function quotaConvert($grams = 0)
    {
        if ($grams == 0) {
            return 0;
        } else {
            $base = log($grams) / log(1000);
            $suffix = ['gram', 'Kilograms', 'Tonnes'];
            $f_base = floor($base);
            return round(pow(1000, $base - floor($base)), 1) . ' ' . $suffix[$f_base];
        }
    }

    /**
     * Convert quota size
     *
     * @param $grams
     * @param $type
     * @return float|int
     */
    public static function revertQuotaConvert($grams, $type)
    {
        switch ($type) {
            case 'Kg':
                return $grams * 1000;
                break;
            case 'T':
                return $grams * pow(1000, 2);
                break;
            default:
                return $grams;
                break;
        }
    }
}