<?php

namespace App\Http\Controllers;

use App\Leaders;
use App\Companies;
use App\Countries;
use Faker\Factory as Faker;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Exception;
use Validator;

class ApiController extends Controller
{
    /**
     * Method for create company row in database
     * if result success app.js append this row to list on the page
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     * @throws \Throwable
     */
    public function createCompany(Request $request)
    {
        $company = new Companies();

        $countries = Countries::getCountriesAsArray();

        if ($request->isMethod('post')) {
            if ($this->validation($request->all(), Companies::rules())) {
                return $this->errorResponse([
                    'message' => 'Fields can not be null!',
                    'success' => false
                ]);
            }

            DB::beginTransaction();

            try {
                $company->name = $request->input('name');
                $company->email = $request->input('email');
                $company->country_id = $request->input('country_id');

                $company->save();

                DB::commit();
            } catch (Exception  $e) {
                DB::rollback();
                return $this->errorResponse([
                    'message' => 'Company not created!',
                    'success' => false
                ]);
            }

            return $this->successResponse([
                'message' => 'Company was created!',
                'method' => 'create',
                'table' => $company->getTable(),
                'html' => view('api.company')->with('obj', $company)->render(),
            ]);
        }

        return view('forms.company', [
            'company' => $company,
            'countries' => $countries,
        ]);
    }

    /**
     * Method for update company row in database and front
     * if result success app.js refreshed this row in list on the page
     *
     * @param Request $request
     * @param integer $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function updateCompany(Request $request, $id)
    {
        $company = Companies::find($id);

        $countries = Countries::getCountriesAsArray();

        if ($request->isMethod('post')) {
            DB::beginTransaction();

            try {
                $company->name = $request->input('name');
                $company->email = $request->input('email');
                $company->country_id = $request->input('country_id');

                $company->save();

                DB::commit();
            } catch (Exception  $e) {
                DB::rollback();
                return $this->errorResponse(['message' => 'Company not updated! Rollback.']);
            }

            return $this->successResponse([
                'message' => 'Company was updated!',
                'method' => 'update',
                'table' => 'companies',
                'data' => [
                    'id' => $id,
                    'name' => $company->name,
                    'email' => $company->email,
                    'company' => $company->country->name,
                ],
            ]);
        }

        return view('forms.company', [
            'company' => $company,
            'countries' => $countries,
        ]);
    }

    /**
     * Delete company row from database and hide on the page
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteCompany($id)
    {
        $company = Companies::where('id', $id)->delete();
        if ($company) {
            return $this->successResponse(['message' => 'Company was deleted!']);
        } else {
            return $this->errorResponse(['message' => 'Company not deleted!']);
        }
    }

    /**
     * Created row in database and append in the page with help app.js
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     * @throws \Throwable
     */
    public function createCountry(Request $request)
    {
        $country = new Countries();

        $types = Countries::types();

        if ($request->isMethod('post')) {
            if ($this->validation($request->all(), Countries::rules())) {
                return $this->errorResponse([
                    'message' => 'Fields can not be null!',
                    'success' => false
                ]);
            }

            DB::beginTransaction();

            try {
                $country->name = $request->input('name');
                $country->quota = Countries::revertQuotaConvert($request->input('quota'), $request->input('quota_type'));
                $country->quota_type = $request->input('quota_type');

                $country->save();

                DB::commit();
            } catch (Exception  $e) {
                DB::rollback();
                return $this->errorResponse(['message' => 'Country not created!']);
            }

            return $this->successResponse([
                'message' => 'Country was created!',
                'method' => 'create',
                'table' => $country->getTable(),
                'html' => view('api.country')->with('obj', $country)->render(),
            ]);
        }

        return view('forms.country', [
            'country' => $country,
            'types' => $types,
        ]);
    }

    /**
     * Update country row in database
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function updateCountry(Request $request, $id)
    {
        $country = Countries::find($id);

        $types = Countries::types();

        if ($request->isMethod('post')) {
            DB::beginTransaction();

            try {
                $country->name = $request->input('name');
                $country->quota = Countries::revertQuotaConvert($request->input('quota'), $request->input('quota_type'));
                $country->quota_type = $request->input('quota_type');

                $country->save();

                DB::commit();
            } catch (Exception  $e) {
                DB::rollback();
                return $this->errorResponse(['message' => 'Country not updated! Rollback']);
            }

            return $this->successResponse([
                'message' => 'Country was updated!',
                'method' => 'update',
                'table' => 'countries',
                'data' => [
                    'id' => $id,
                    'name' => $request->input('name'),
                    'quota' => $request->input('quota'),
                    'quota_type' => $types[$request->input('quota_type')],
                ],
            ]);
        }

        return view('forms.country', [
            'country' => $country,
            'types' => $types,
        ]);
    }

    /**
     * Delete row from database, and later delete from page if operation success
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteCountry($id)
    {
        $company = Countries::where('id', $id)->delete();
        if ($company) {
            return $this->successResponse(['message' => 'Country was deleted!']);
        } else {
            return $this->errorResponse(['message' => 'Country not deleted!']);
        }
    }

    /**
     * Generate custom data with Faker
     * Used https://github.com/fzaninotto/Faker
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function generate()
    {
        $faker = Faker::create();

        DB::beginTransaction();

        try {
            for ($i = 1; $i <= 500; $i++) {
                $model = new Leaders();

                $model->company_id = DB::table('companies')->inRandomOrder()->first()->id;
                $model->date_time = $faker->dateTimeBetween('-12 month', 'now')->getTimestamp();
                $model->mined = $faker->numberBetween($min = 100, $max = 10000000);

                $model->save();

                DB::commit();
            }
        } catch (Exception  $e) {
            DB::rollback();
            return $this->errorResponse(['message' => 'Some went wrong with generate!']);
        }

        $result = Leaders::limit(20)->get();

        return $this->successResponse([
            'message' => 'Random data was generated!',
            'html' => view('api.generate')->with('result', $result)->render(),
        ]);
    }

    /**
     * Show more action, result append to page
     *
     * @param $page
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function more($page)
    {
        $offset = ($page - 1) * 20;

        $result = Leaders::skip($offset)->take(20)->get();

        if (count($result)) {
            return $this->successResponse([
                'html' => view('api.leaders')->with('result', $result)->render(),
                'page' => $page + 1,
            ]);
        } else {
            return $this->errorResponse();
        }
    }

    /**
     * Generate report for current month
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function getReport(Request $request)
    {
        $month = $request->input('month');

        $report = DB::table('leaders')->select(
            DB::raw('MONTH(FROM_UNIXTIME(leaders.date_time)) AS month'),
            DB::raw('SUM(leaders.mined) AS mined'),
            DB::raw('countries.quota AS quota'),
            DB::raw('countries.name AS country_name')
        )
            ->leftJoin('companies', 'companies.id', '=', 'leaders.company_id')
            ->leftJoin('countries', 'countries.id', '=', 'companies.country_id')
            ->groupBy('month', 'country_name')
            ->having('month', '=', $month)
            ->orderBy('month', 'asc')
            ->get();

        if (count($report)) {
            return $this->successResponse([
                'result' => view('api.report')
                    ->with('report', $report)
                    ->with('month', $month)
                    ->with('months', Leaders::month())
                    ->render(),
            ]);
        } else {
            return $this->errorResponse(['message' => 'Report not created!']);
        }
    }

    /**
     * Validation
     *
     * @param $request
     * @param $rules
     * @return bool
     */
    public function validation($request, $rules)
    {
        $validator = Validator::make($request, $rules);
        if ($validator->fails()) {
            return true;
        } else {
            return false;
        }
    }
}