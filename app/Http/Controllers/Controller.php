<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function successResponse($response = null)
    {
        $response['success'] = true;

        return response()->json($response, 200, [], JSON_NUMERIC_CHECK);
    }

    public function errorResponse($error = null)
    {
        $response['success'] = false;

        if ($error) {
            $response = $error;
        }

        return response()->json($response, 200, [], JSON_NUMERIC_CHECK);
    }
}
