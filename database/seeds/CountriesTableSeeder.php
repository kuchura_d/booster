<?php

use Illuminate\Support\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $quotaType = [
            1 => 'Kg',
            2 => 'T',
        ];

        $faker = Faker::create();

        for ($i = 1; $i <= 10; $i++) {
            $quota = $faker->numberBetween($min = 1000, $max = 30000000);

            DB::table('countries')->insert([
                'name' => $faker->country,
                'quota' => $quota,
                'quota_type' => self::getQuotaType($quota),
                'created_at' => $faker->dateTimeThisMonth('now', 'Europe/Kiev'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }
    }

    public function getQuotaType($value = 0)
    {
        $base = log($value) / log(1000);
        $type = ['g', 'Kg', 'T'];
        $f_base = floor($base);

        return $type[$f_base];
    }
}