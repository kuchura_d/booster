<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'company'], function () {
    Route::get('/create', 'ApiController@createCompany');
    Route::post('/create', 'ApiController@createCompany');
    Route::get('/update/{id}', 'ApiController@updateCompany');
    Route::post('/update/{id}', 'ApiController@updateCompany');
    Route::get('/delete/{id}', 'ApiController@deleteCompany');
});

Route::group(['prefix' => 'country'], function () {
    Route::get('/create', 'ApiController@createCountry');
    Route::post('/create', 'ApiController@createCountry');
    Route::get('/update/{id}', 'ApiController@updateCountry');
    Route::post('/update/{id}', 'ApiController@updateCountry');
    Route::get('/delete/{id}', 'ApiController@deleteCountry');
});

Route::get('/generate', 'ApiController@generate');
Route::get('/show-more/{page}', 'ApiController@more');
Route::post('/report', 'ApiController@getReport');