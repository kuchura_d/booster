<?php

use App\Companies;

class CompanyCest
{
    public function _before(UnitTester $I)
    {
    }

    public function _after(UnitTester $I)
    {
    }

    public function deleteCompanyTrue(UnitTester $I)
    {
        $id = 1;

        $I->seeRecord('companies', ['id' => $id]);
        Companies::where('id', $id)->delete();
        $I->cantSeeRecord('companies', ['id' => $id]);
    }

    public function deleteCompanyFalse(UnitTester $I)
    {
        $id = 777;

        $I->cantSeeRecord('companies', ['id' => $id]);
        $result = Companies::where('id', $id)->delete();
        $I->assertArrayHasKey('error', $result);
    }
}
