<?php


class IndexCest
{
    public function _before(AcceptanceTester $I)
    {
    }

    public function _after(AcceptanceTester $I)
    {
    }

    public function homePageTest(AcceptanceTester $I)
    {
        $I->wantTo('Check home page');
        $I->amOnPage('/');
        $I->see('Laravel');
    }
}
