<?php

use App\Countries;

/* @var $obj \App\Countries */
?>

{!! Form::open(['method' => 'post', 'id' => 'country-form', 'class' => 'form-ajax']) !!}

<div class="form-group">
    {{ Form::label('name', 'Name', ['class' => 'control-label']) }}
    {{ Form::text('name', isset($country->name) ? $country->name : '', ['class' => 'form-control', 'require' => true]) }}
</div>

<div class="form-group">
    {{ Form::label('quota', 'Quota', ['class' => 'control-label']) }}
    {{ Form::text('quota', isset($country->quota) ? (int)Countries::quotaConvert($country->quota) : 0, ['class' => 'form-control', 'require' => true]) }}
</div>

<div class="form-group">
    {{ Form::label('quota_type', 'Quota type', ['class' => 'control-label']) }}
    {{ Form::select('quota_type', $types, isset($country->quota_type) ? $country->quota_type : '', ['class' => 'form-control', 'placeholder' => 'Choose quota type', 'require' => true]) }}
</div>

<div class="form-group">
    {{ Form::submit(isset($country->id) ? 'Update' : 'Create', ['class' => 'btn btn-primary']) }}
</div>

{!! Form::close() !!}