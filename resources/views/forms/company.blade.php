<?php /* @var $company \App\Companies */ ?>

{!! Form::open(['method' => 'post', 'id' => 'company-form', 'class' => 'form-ajax']) !!}

<div class="form-group">
    {{ Form::label('name', 'Name', ['class' => 'control-label']) }}
    {{ Form::text('name', isset($company->name) ? $company->name : '', ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('email', 'Email', ['class' => 'control-label']) }}
    {{ Form::email('email', isset($company->email) ? $company->email : '', ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('country_id', 'Country', ['class' => 'control-label']) }}
    {{ Form::select('country_id', $countries, isset($company->country_id) ? $company->country_id : '', ['class' => 'form-control', 'placeholder' => 'Choose country']) }}
</div>

<div class="form-group">
    {{ Form::submit(isset($company->id) ? 'Update' : 'Create', ['class' => 'btn btn-primary']) }}
</div>

{!! Form::close() !!}