<?php

use App\Countries;

/* @var $result Countries */

?>

@foreach ($result as $obj)
    <tr>
        <td>{{ isset($obj->company->name) ? $obj->company->name : '<strong>Company deleted!</strong>' }}</td>
        <td>{{ date('d M Y H:i:s', $obj->date_time) }}</td>
        <td>{{ Countries::quotaConvert($obj->mined) }}</td>
    </tr>
@endforeach