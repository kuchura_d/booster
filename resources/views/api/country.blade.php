<?php

use App\Countries;

/* @var $obj \App\Countries */
?>

<li class="list-group-item item__block">
    <div class="item__text"
         id="{{ 'country-' . $obj->id }}">{{ $obj->name . ' / ' . Countries::quotaConvert($obj->quota) }}</div>
    <div class="item__buttons">
        <button class="btn btn-info modal-btn"
                data-target="{{ 'api/country/update/' . $obj->id }}">
            Edit
        </button>
        <button class="btn btn-danger deleteButton"
                data-alias="{{ 'api/country/delete/' . $obj->id }}">
            Delete
        </button>
    </div>
</li>