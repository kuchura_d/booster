<?php /* @var $obj \App\Companies */ ?>

<li class="list-group-item item__block">
    <div class="item__text" id="{{ 'company-' . $obj->id }}">
        {{ $obj->name . ' / ' . $obj->email . ' / ' }}
        {{ isset($obj->country->id) ? $obj->country->name : 'Country deleted!' }}
    </div>
    <div class="item__buttons">
        <button class="btn btn-info modal-btn"
                data-target="{{ 'api/company/update/' . $obj->id }}">
            Edit
        </button>
        <button class="btn btn-danger deleteButton"
                data-alias="{{ 'api/company/delete/' . $obj->id }}">
            Delete
        </button>
    </div>
</li>