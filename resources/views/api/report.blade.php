<?php

use App\Countries;

/* @var $report \App\Leaders */
/* @var $months [] */
/* @var $month int */

?>

<h1>{{ 'Report for ' . $months[$month] }}</h1>
@if ($report)
    <ul id="report" class="list-group reportData">
        @foreach ($report as $obj)
            @if ($obj->mined > $obj->quota)
                <li class="list-group-item usersData-item">
                    <div class="usersData-item__text">
                        <span>{{ isset($obj->country_name) ? $obj->country_name : 'Country deleted!' }}</span>
                        <span>{{ ' / '.Countries::quotaConvert($obj->mined) . ' / ' }}</span>
                        <span><strong>{{ 'Limit: [' . Countries::quotaConvert($obj->quota) . ']' }}</strong></span>
                    </div>
                </li>
            @endif
        @endforeach
    </ul>
@endif