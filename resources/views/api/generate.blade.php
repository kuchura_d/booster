<?php

use App\Countries;

/* @var $result Countries */

?>

<table class="table">
    <thead>
    <tr>
        <th>Company</th>
        <th>Date/time</th>
        <th>Mined</th>
    </tr>
    </thead>
    <tbody id="table-body">
    @foreach ($result as $obj)
        <tr>
            <td>{{ isset($obj->company->name) ? $obj->company->name : '<strong>Company deleted!</strong>' }}</td>
            <td>{{ date('d M Y H:i:s', $obj->date_time) }}</td>
            <td>{{ Countries::quotaConvert($obj->mined) }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
{{ Form::button('Show more', ['class' => 'btn btn-success showMore']) }}
{{ Form::hidden('page', 2, ['id' => 'page']) }}