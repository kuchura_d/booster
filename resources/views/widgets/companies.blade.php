<?php /* @var $obj \App\Companies */ ?>

<a href="#" class="btn btn-success modal-btn" data-target="api/company/create">Add / Create</a>
<hr>
<ul id="companiesData" class="list-group companiesData">
    @if (count($result))
        @foreach ($result as $obj)
            <li class="list-group-item item__block">
                <div class="item__text" id="{{ 'company-' . $obj->id }}">
                    {{ $obj->name . ' / ' . $obj->email . ' / ' }}
                    {{ isset($obj->country->id) ? $obj->country->name : 'Country deleted!' }}
                </div>
                <div class="item__buttons">
                    <button class="btn btn-info modal-btn"
                            data-target="{{ 'api/company/update/' . $obj->id }}">
                        Edit
                    </button>
                    <button class="btn btn-danger deleteButton"
                            data-alias="{{ 'api/company/delete/' . $obj->id }}">
                        Delete
                    </button>
                </div>
            </li>
        @endforeach
    @else
        <div class="alert alert-warning" role="alert">
            <p>No data companies to view. Please create user click to <strong>Add / Create</strong> button.</p>
        </div>
    @endif
</ul>