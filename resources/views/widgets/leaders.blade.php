<?php

use App\Countries;

/* @var $result \App\Leaders */

?>


<div class="row">
    {{ Form::button('Generate Data', ['class' => 'btn btn-default generateData']) }}

    {{ Form::select('month', $month, null, ['class' => 'monthSelect', 'placeholder' => 'Choose month']) }}
    {{ Form::button('Generate Report', ['class' => 'btn btn-default generateReport']) }}
</div>
<div id="leaders">
    <h1>Leaders</h1>
    @if (count($result))
        <table class="table">
            <thead>
            <tr>
                <th>Company</th>
                <th>Date/time</th>
                <th>Mined</th>
            </tr>
            </thead>
            <tbody id="table-body">
            @foreach ($result as $obj)
                <tr>
                    <td>{{ isset($obj->company->name) ? $obj->company->name : '<strong>Company deleted!</strong>' }}</td>
                    <td>{{ date('d M Y H:i:s', $obj->date_time) }}</td>
                    <td>{{ Countries::quotaConvert($obj->mined) }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ Form::button('Show more', ['class' => 'btn btn-success showMore']) }}
        {{ Form::hidden('page', 2, ['id' => 'page']) }}
    @else
        <div class="alert alert-warning" role="alert">
            <p>No data to show. Please, press <strong>Generate Data</strong>!</p>
        </div>
    @endif
</div>
<div class="report" id="report"></div>