<?php

use App\Countries;

/* @var $obj \App\Countries */
?>

<a href="#" class="btn btn-success modal-btn" data-target="api/country/create">Add / Create</a>
<hr>
<ul id="countriesData" class="list-group countriesData">
    @if (count($result))
        @foreach ($result as $obj)
            <li class="list-group-item item__block">
                <div class="item__text"
                     id="{{ 'country-' . $obj->id }}">{{ $obj->name . ' / ' . Countries::quotaConvert($obj->quota) }}</div>
                <div class="item__buttons">
                    <button class="btn btn-info modal-btn"
                            data-target="{{ 'api/country/update/' . $obj->id }}">
                        Edit
                    </button>
                    <button class="btn btn-danger deleteButton"
                            data-alias="{{ 'api/country/delete/' . $obj->id }}">
                        Delete
                    </button>
                </div>
            </li>
        @endforeach
    @else
        <div class="alert alert-warning" role="alert">
            <p>No data countries to view. Please create company click to <strong>Add / Create</strong> button.</p>
        </div>
    @endif
</ul>