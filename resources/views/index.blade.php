<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="language" content="en">
    <link type="text/css" href="{{ asset('css/app.css') }}" rel="stylesheet">
    <title>Laravel</title>
</head>
<body>
<div class="wrap">
    <nav class="navbar-inverse navbar-fixed-top navbar" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <a href="{{ url('/') }}" class="navbar-brand">Laravel</a>
            </div>
            <div class="collapse navbar-collapse"></div>
        </div>
    </nav>
    <div class="container">
        <div>
            <div class="tabbable-line tabbable-full-width">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#users" data-toggle="tab" aria-expanded="true"> Companies </a>
                    </li>
                    <li class="">
                        <a href="#companies" data-toggle="tab" aria-expanded="false"> Country </a>
                    </li>
                    <li class="">
                        <a href="#abusers" data-toggle="tab" aria-expanded="false"> Leaders </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="users">
                        <div class="row">
                            {{--Companies WIDGET--}}
                            {{ Widget::companies() }}
                        </div>
                    </div>
                    <div class="tab-pane" id="companies">
                        <div class="row">
                            {{--Country WIDGET--}}
                            {{ Widget::countries() }}
                        </div>
                    </div>
                    <div class="tab-pane" id="abusers">
                        <div class="row">
                            {{--Leaders WIDGET--}}
                            {{ Widget::leaders() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{ Widget::popup() }}
    </div>
</div>
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
